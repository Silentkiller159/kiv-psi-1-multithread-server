# KIV-PSI-2-Multithread-server

## Description of logic
- Creates listening socket on port 5000 (Any ip address)
- If connection is accepted, new thread is created for handling and main loop waits for another potentional connection
- Thread handles the request, is request type is GET, it is handled (Otherwise dropped)
- Then, request path is evaluated, if path is not found on this server, 404 response is created
- If path is found, correct response is served ("/" and "/about" is prepared for testing)
- Thread ends its life then

## How to use
- Makefile is present, so running "make" in src folder should create binary file called "webserver"
- (Alternatively, without make, can be compiled using: "gcc -pthread main.c -o webserver")
- Binary file can be run using "./webserver"

## Dependencies
- Linux
- gcc
- Make