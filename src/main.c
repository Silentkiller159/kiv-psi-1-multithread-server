#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <pthread.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>

const int MAX_THREADS = 10;
const int SERVER_PORT = 5000;
const char* METHOD_GET = "GET";
const char* RESPONSE_OK = "HTTP/1.1 200 OK";
const char* RESPONSE_NOT_FOUND = "HTTP/1.1 404 Not Found";

const int MESSAGE_BUFFER = 1024;
const int TEXT_LENGTH_CHAR_LIMIT = 10;

const char* PAGE_HEADER = "<html><head><link rel=\"shortcut icon\" href=\"https://silent-industries.cz/logo_black.png\"><link rel=\"stylesheet\" href=\"https://silent-industries.cz/assets/css/main.css\"></head><body>";
const char* PAGE_FOOTER = "</body></html>";

void generate_response(int socket_fd, const char* response_type, const char* text){

    write(socket_fd, response_type, strlen(response_type));
	write(socket_fd, "\r\n", 2);

    //Text of response
    char length_str[TEXT_LENGTH_CHAR_LIMIT];
	sprintf(length_str, "%d", (int)(strlen(PAGE_HEADER) + strlen(text) + strlen(PAGE_FOOTER)));
    char* text_length_str = (char*)malloc(strlen("Content-Length: ") + strlen(length_str) + 1);
    strcpy(text_length_str, "Content-Length: ");
    strcpy(text_length_str, length_str);

    //MESSAGE
    write(socket_fd, "Server: Example/1.0", strlen("Server: Example/1.0"));
	write(socket_fd, "\r\n", 2);
    write(socket_fd, "Content-Type: text/html", strlen("Content-Type: text/html"));
	write(socket_fd, "\r\n", 2);
    write(socket_fd, text_length_str, strlen(text_length_str));
	write(socket_fd, "\r\n", 2);
    write(socket_fd, "", strlen(""));
	write(socket_fd, "\r\n", 2);

    write(socket_fd, PAGE_HEADER, strlen(PAGE_HEADER));
    write(socket_fd, text, strlen(text));
    write(socket_fd, PAGE_FOOTER, strlen(PAGE_FOOTER));
}

char* get_path(char* text) {

    int path_offset = strlen(METHOD_GET) + 1;
	int end_pos = strchr(text + path_offset, ' ') - text;
	int len = end_pos - path_offset;

	char* path_text = (char*)malloc(len + 1);
	strncpy(path_text, text + path_offset, len);
	path_text[len] = '\0';
	return path_text;
}

char* get_message(int sockfd) {
	
    char* buffer = (char*)malloc(MESSAGE_BUFFER);
	char* result = (char*)malloc(1);
	result[0] = '\0';
	int read_len;
	while(1) {
		int read_len = read(sockfd, buffer, MESSAGE_BUFFER - 1);
		if(read_len < 0) {
            perror("ERROR: While reading from socket");
            exit(1);
        }
		buffer[read_len] = '\0';
		char* last_result = result;
        result = (char*)malloc(strlen(last_result) + strlen(buffer) + 1);
        strcpy(result, last_result);
        strcpy(result, buffer);
		free(last_result);
		if(read_len < MESSAGE_BUFFER - 1) {
            break;
        }
	}
	free(buffer);
	return result;
}

void req_handle_in_thread(void* argument){
    int socket_fd = *((int*)argument); //Gimme int

	printf("Processing socket: %d\n", socket_fd);
	char* text = get_message(socket_fd);
	printf("Message: %s\n\n", text);
	if(strncmp(text, METHOD_GET, strlen(METHOD_GET)) == 0) { 
        //Its GET handle this
		char* path = get_path(text);
		printf("Path: %s\n", path);

		if(strcmp(path, "/") == 0) {
            generate_response(socket_fd, RESPONSE_OK, "<h1>Welcome, all systems activated</h1><p>This is an example of a basic web server</p><a href=\"flemch\">Non-existing page</a></br><a href=\"about\">About page</a>");
        }
		else if(strcmp(path, "/about") == 0) {
            generate_response(socket_fd, RESPONSE_OK, "<h1>About page</h1><p>Created by Milan Hotovec</br>As an example of basic web server written in C using BSD sockets.</br>(c) Silent 2021</p>");
        }
		else {
            //404
            generate_response(socket_fd, RESPONSE_NOT_FOUND, "<h1>404 NOT FOUND!</h1>");
        }

		free(path);
	}
	else {
        //Its not GET ... BYE
    }
	free(text);
	close(socket_fd);
	free(argument);
}


int init_listen(){

    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_fd < 0) {
            perror("ERROR: While creating listen socket!");
            exit(1);
    }

	int setopt = 1; //Allow reusing address
	if(-1 == setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, (char*)&setopt, sizeof(setopt))){
        perror("ERROR setting socket options!");
        exit(1);
    }
		
	struct sockaddr_in server_addr;
	bzero(&server_addr, sizeof(server_addr)); //ERASE mem
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(SERVER_PORT);
	server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	if(bind(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr)) < 0){
        fprintf(stderr, "ERROR while binding socket with port: %d (Already used?)", SERVER_PORT, strerror(errno));
        exit(1);
    }

	if(listen(socket_fd, SOMAXCONN) < 0){
        perror("ERROR while initiating listen!");
        exit(1);
    }
	printf("Server started on port: %d\n", SERVER_PORT);

	return socket_fd;
}

int main() {

    printf("Program started..\n\n");
    //Threads
    pthread_t thr[MAX_THREADS];
	struct sockaddr_in client_addr;
    int client_size = sizeof(client_addr);
	int i = 0;
    int listen_sock_fd = init_listen();

	while (1) {
		int req_handle_sock_fd = accept(listen_sock_fd, (struct sockaddr*)&client_addr, (socklen_t*)&client_size);

		if(req_handle_sock_fd < 0) {
            perror("ERROR: While creating handle socket");
            exit(1);
        }
		printf("New request accepted: %d\n", i);

		int* req_handle_sock_fd_pointer = (int*)malloc(sizeof(int));
		*req_handle_sock_fd_pointer = req_handle_sock_fd;
		pthread_create(&thr[i++], NULL, req_handle_in_thread, req_handle_sock_fd_pointer);
	}
	close(listen_sock_fd);

    return 0;
}